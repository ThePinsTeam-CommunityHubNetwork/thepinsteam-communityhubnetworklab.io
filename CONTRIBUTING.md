# Contribution Guide
All of your contributions, whenever you use our Issue Trackers or not,
must follow our [Community Network Code of Conduct].

[Community Network Code of Conduct]: https://en.handbooksbythepins.gq/community-hub/policies#issue-trackers

## Reporting Violations
To report violations, see [our Reporting Guide]. For grievances, see [our guide on reporting grievances].

[our Reporting Guide]: https://en.handbooksbythepins.gq/community-hub/report-violations
[our guide on reporting grievances]: https://en.handbooksbythepins.gq/community-hub/report-violations#grievances

## Basic Rules
* **Stay on the topic.** Use our Issue Tracker in the `ThePinsTeam-CommunityHubNetwork/general` for general discussion.
* **Respect others' opinion.** Having a freedom on expression without moderation can cause chaos, so we need to moderate them. In case of emergency, don't forget to mention the maintainers and use GitLab's *Report Abuse** button.
