# Network Website
You are here in the `gitlab.com:ThePinsTeam-CommunityHubNetwork/thepinsteam-communityhubnetwork.gitlab.io.git` repository, where the community network site are hosted.

## Maintainers
| Maintainer Name | GitLab Username | Maintainership Status | Maintainer Since |
| ----- | ----- | ----- | ----- |
| Andrei Jiroh Eugenio Halili | @AndreiJirohHaliliDev2006 | **Active/The Pins Team Member** | Early days of the Network Site |

### Apply for Maintainership
See `source/maintainership/apply.md` or in [our English Handbook](https://en.handbooksbythepins.gq/community-hub/fandoms/issue-trakcers/maintainerships#application) for details.

## Contribute
See `CONTRIBUTING.md` for details on how you can contribute to the website.
